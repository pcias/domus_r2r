#!/usr/bin/env node

var argv = require('optimist')
    .usage('Usage: $0 -username [num] -password [num]')
    .demand(['username','password'])
    .argv;
    
var user = require('./modules/user');
user.addUser(argv.username,argv.password);    
console.log('User '+argv.username+' added. If you wish to remove - delete directly from database/settings/passwd');