#include <LiquidCrystal.h>
#include <Process.h>

//will need to add a watchdog here

//application data
#define WEBTEMP 0
#define MANTEMP 1
#define USUTEMP 2

#define MINTEMP 10
#define MAXTEMP 30

#define LOGFREQ 600000
#define ACTFREQ 3100

#define USERNAME "sensor"
#define PASSWORD "sensor1data"

//#define DOMUS_R2_URL "https://domus_r2-c9-pcias.c9.io"
#define DOMUS_R2_URL "http://pcsbotaniczna.no-ip.org:3000"
//#define DOMUS_R2_URL "http://192.168.1.126:3000"
#define SENSOR_ID "YUN_LCD_Controller"

volatile int manTemp=20;  //temperature set from com or by buttons 
volatile int setman = 0;  //manually set by button recently

int webTemp;
String displayStr;
volatile int tempSource = WEBTEMP;   //0 WEB (timers) -> 1 MAN (manual) -> 2 USU (usual temperature this hour and day of week)




LiquidCrystal lcd(8, 9, 4, 5, 6, 7); //konfigurowanie linii do których został dołączony LCD

const int Led1 = 13; //przypisanie aliasów do pinów portów
const int Led2 = 12; 
const int Led3 = 11; 
const int Led4 = 10; 
const int SW1 = 3; 
const int SW2 = 2; 
const int SW3 = 1; 
const int SW4 = 0; 
const int Buzzer = A5; 


void setup() {
  Bridge.begin();
  lcd.begin(16,2);
  //lcd.autoscroll();
  lcd.setCursor(0,0);
  pinMode(Led1, OUTPUT); //konfigurowanie I/O, do których są
  //dołączone diody LED
  pinMode(Led2, OUTPUT);
  pinMode(Led3, OUTPUT);
  pinMode(Led4, OUTPUT);
  pinMode(Buzzer, OUTPUT); //konfigurowanie I/O, do której jest
  //dołączony brzęczyk piezzo
  pinMode(SW1, INPUT); //konfigurowanie I/O, do których są
  //dołączone przyciski
  pinMode(SW2, INPUT);
  pinMode(SW3, INPUT);
  pinMode(SW4, INPUT);
 
  digitalWrite(Led1, HIGH);
  digitalWrite(Led2, HIGH);
  digitalWrite(Led3, HIGH);
  digitalWrite(Led4, HIGH);
  
  digitalWrite(Buzzer, HIGH);
  
  // Open serial communications and wait for port to open:
  //Serial.begin(9600);
  // while (!Serial) {
  //  ; // wait for serial port to connect. Needed for Leonardo only
  //}  
  
  
  
  attachInterrupt(0, man_minus, FALLING); //pin 2 = SW2
  attachInterrupt(1, man_plus, FALLING);  //pin 3 = SW1
  //attachInterrupt(3, web_man, FALLING);  //pin 1 = SW3 //does not work
}


String linecom;

void loop() {  
  //send commands up/temp
  long int m;
  m = millis();
  
  if(!(m % ACTFREQ)) {
      //Serial.print("TEM:");
      //Serial.println(temperature());
      dispTemp();
      getBanner();
      sendTemp();
      getWebTemperature();
      getHeating();
      getBoilerStatus();
      if(!setman) {      //ie. was not recently set by button
        getTemperatureSource();  
      }
      else
      {
          setman = 0;
          sendTempSource();
      }
      dispStr();
  }  
  
  //every ca 5 minutes log temperature
  if(!(m % LOGFREQ)) {
    logTemp();
  }
  
  if(!(m%100)) {
//    if(digitalRead(SW1) == LOW) {
//      manTemp>MINTEMP? manTemp-- : 10;
//      //delay(50);
//      tempSource = MANTEMP;
//      //sendTempSource();
//      dispTemp();
//    }
//    if(digitalRead(SW2) == LOW) {
//      manTemp<MAXTEMP? manTemp++ : 30;
//      tempSource = MANTEMP;
//      //sendTempSource();
//      dispTemp();
//    }    
    if(digitalRead(SW3) == LOW) {
      web_man(); 
    }
    if(digitalRead(SW4) == LOW) {
      displayStr = "Will be away...";
      dispStr();
      delay(100);
    }    
  }
}


void man_plus() {
      manTemp<MAXTEMP? manTemp++ : 30;
      tempSource = MANTEMP;
      setman = 1;
      dispTemp();
}

void man_minus() {
      manTemp>MINTEMP? manTemp-- : 10;
      tempSource = MANTEMP;
      setman = 1;
      dispTemp();
}

void web_man(){
      tempSource=(tempSource+1)%2;   //would be %3 but USU removed
      sendTempSource();
      dispTemp();
}

//int executeCommand(String command) {
//
//  if(command.startsWith("DO+DISP:")) {
//    displayStr = command.substring(8);
//    dispStr();
//    return 1;
//  }
//  
//  if(command.startsWith("DO+SETT:")) {
//    
//    webTemp = command.substring(8).toInt();
//    return 1;
//  }
//  
//  return 0; //fail
//}


void dispTemp() {
    lcd.setCursor(0,1);
    switch(tempSource) {
      case 0:
        lcd.print("WEB:");
        lcd.print("    ");
        lcd.setCursor(4,1);
        lcd.print(webTemp);
        break;
      case 1:
        lcd.print("MAN:");
        lcd.print("    ");
        lcd.setCursor(4,1);
        lcd.print(manTemp);
        break;
//      case 2:
//        lcd.print("USU:");
//        lcd.print("    ");
//        lcd.setCursor(4,1);
//        lcd.print(usuTemp);  
    }
    

    lcd.setCursor(8,1);
    lcd.print("ACT:");
    lcd.print("    ");
    lcd.setCursor(12,1);
    lcd.print((int)temperature());
}

void dispStr() {
  lcd.setCursor(0,0);
  lcd.print("                ");
  lcd.setCursor(0,0);
  lcd.print(displayStr);
}

void getBanner() {
  Process p;
  //digitalWrite(Led2, LOW);
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/banner");
  p.run();
  // Print command output on the Serial.
  // A process output can be read with the stream methods
  displayStr = "";
  int i=0;
  while (p.available()>0 && i < 16) {
    char c = p.read();
    displayStr+=c;  
    i++;
  }
  //digitalWrite(Led2, HIGH);  
}

void getBoilerStatus() {
  Process p;
  String status;
  
  //digitalWrite(Led2, LOW);
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/boilerstatus");
  p.run();
  int i  = 0;
  while (p.available()>0 && i < 1) {
    char c = p.read();
    status+=c;  
    i++;
  }
  
  if(status[0]=='f') {
    digitalWrite(Buzzer, LOW);
    delay(25);
    digitalWrite(Buzzer, HIGH);
  }
  //digitalWrite(Led2, HIGH);
}

void getHeating() {
  Process p;
  String status;
  
  
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/heating");
  p.run();
  int i  = 0;
  while (p.available()>0 && i < 1) {
    char c = p.read();
    status+=c;  
    i++;
  }
  
  if(status[0]=='t') 
    digitalWrite(Led1, LOW);
  else 
    digitalWrite(Led1, HIGH);
}


void getTemperatureSource() {
  Process p;
  String status;
  
  //digitalWrite(Led4, LOW);
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/temperature_source");
  p.run();
  int i  = 0;
  while (p.available()>0 && i < 1) {
    char c = p.read();
    status+=c;  
    i++;
  }
  
  if(status[0]=='W') {
    tempSource =  WEBTEMP;
  }
  else {
    tempSource = MANTEMP;
  }
  //digitalWrite(Led4, HIGH);
}

void getWebTemperature() {
  Process p;
  String status;
  
  //digitalWrite(Led3, LOW);
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter(String(DOMUS_R2_URL)+"/web_temperature");
  p.run();
  status = "";
  int i = 0;
  while (p.available()>0 && i < 5) {
    char c = p.read();
    status+=c;  
    i++;
  }
  webTemp = status.toInt();
  //digitalWrite(Led3, HIGH);
}


void logTemp() {
  Process p;
  //digitalWrite(Led1, LOW);
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter("--data");
  p.addParameter(String("sensor_id=")+SENSOR_ID+"&actual_temperature="+temperature()+"&manual_temperature="+manTemp+"&temperature_source="+(tempSource==WEBTEMP?"WEB":"MAN"));
  //p.addParameter(String("sensor_id=")+"comment&actual_temperature=23&manual_temperature=20&temperature_source=MAN");
  p.addParameter(String(DOMUS_R2_URL)+"/templogger");
  p.run();
  //digitalWrite(Led1, HIGH);
}

void sendTemp() {
  Process p;
  //digitalWrite(Led1, LOW);
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter("--data");
  //p.addParameter("sensor_id=yun&actual_temperature=20");
  p.addParameter(String("sensor_id=")+SENSOR_ID+"&actual_temperature="+temperature()+"&manual_temperature="+manTemp);
  p.addParameter(String(DOMUS_R2_URL)+"/settings");
  p.run();
  //digitalWrite(Led1, HIGH);
}

void sendTempSource() {
  Process p;
  //digitalWrite(Led1, LOW);
  p.begin("curl");
  p.addParameter("-u"); 
  p.addParameter(String(USERNAME)+":"+PASSWORD);
  p.addParameter("-k");
  p.addParameter("--data");
  //p.addParameter("sensor_id=yun&actual_temperature=20");
  p.addParameter(String("sensor_id=")+SENSOR_ID+"&temperature_source="+(tempSource==WEBTEMP?"WEB":"MAN"));
  p.addParameter(String(DOMUS_R2_URL)+"/settings");
  p.run();
  //digitalWrite(Led1, HIGH);
}

float temperature() {
  int sensor_val = 0;
  const int count = 5;
  
  for (int i = 0; i < count ; i++) { //average of count readings
    sensor_val = sensor_val + analogRead(A1);
    delay(10);
  }
  return (4.4*sensor_val*100.0/count)/1024.0;
  
//    return (4.4*analogRead(A1)*100.0)/1024.0;
}

