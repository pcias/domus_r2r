//cron-like or processing's loop

var Store = require('ministore')('./database/settings');
var net = require('net');
var heating, global;

function actTemp() {
    var globalsStore = Store('globals');
    return parseFloat(globalsStore.get("actual_temperature"));
}

function boilerConnect(boilerCmd){
    var globalsStore = Store('globals');
    //if testing env
    if(globalsStore.get("boilerIP")=="0.0.0.0") {
        var globalsStore = Store('globals');
        globalsStore.set("boiler_connection_ok",false);
        return;
    }
        
    //otherwise production    
    var connection = net.connect(globalsStore.get("boilerPORT"), globalsStore.get("boilerIP"), function() {
        var date = new Date();
        console.log(date.toString()+'domus_r2: connection to arduino SUCCESS');
        connection.write(boilerCmd);
        var globalsStore = Store('globals');
        globalsStore.set("boiler_connection_ok",true);
    });
    
    connection.on('error', function() {
            var date = new Date();
            
            console.log(date.toString()+'domus_r2: connection to arduino ERROR');
            globalsStore.set("boiler_connection_ok",false);
    });
    
    connection.on('data', function(data) {
        console.log('domus_r2: arduino response: '+data.toString());
        var globalsStore = Store('globals');
        globalsStore.set("boiler_connection_ok",true);
        connection.end();
    });
}


function heatingOn(){
    var globalsStore = Store('globals');
    //globalsStore.get("heatingOnCmd", function(err,data) { if(!err) { boilerConnect(data); }; } );
    globalsStore.set("heating",true);
    heating =  true;
    console.log("heating on");
}

function heatingOff(){
    var globalsStore = Store('globals');
    //boilerConnect(globalsStore.get("heatingOffCmd"));
    heating =  false;
    globalsStore.set("heating",false);
    console.log("heating off");
}

function waterOn(){
    var globalsStore = Store('globals');
    //boilerConnect(globalsStore.get("waterOnCmd"));
    global =  true;
    globalsStore.set("global",true);
    console.log("global on");
}

function waterOff(){
    var globalsStore = Store('globals');
    //boilerConnect(globalsStore.get("waterOffCmd"));
    global =  false;
    globalsStore.set("global",false);
    console.log("global off");
}





function loop() {

  function actuate(act,st) {
        var globalsStore = Store('globals');
        
         //a little hysteresis
         if(act < st - 0.5) {
              heatingOn();
          }
          else if (act >= st + 0.5 ) {
              heatingOff();    
          }
          //needs repeating boiler commands in case arduino resets
          if(global) 
            boilerConnect(globalsStore.get("waterOnCmd"));
          else 
            boilerConnect(globalsStore.get("waterOffCmd"));
          
          if(heating) 
            boilerConnect(globalsStore.get("heatingOnCmd"));
          else 
            boilerConnect(globalsStore.get("heatingOffCmd"));
  }

  var date = new Date();
  var globalsStore = Store('globals');
  var hoursStore = Store(globalsStore.get('profile'));
  var hours = date.getHours();
  var act = actTemp(); 
  var st;
  
  hoursStore.get(hours, function(err,data) {
          var ts = globalsStore.get("temperature_source");
          
          if(ts=="WEB") {
            st =parseInt(data.temp);
            globalsStore.set("web_temperature",st);
          }
          if(ts=="MAN") {
            st = parseInt(globalsStore.get("manual_temperature")); 
          }
          
          actuate(act,st);
          
          //water always comes from WEB
          if(data.water) {
              waterOn();
          }
          else {
              waterOff();
          }
    });
}

exports.loop =  loop;
