var Store = require('ministore')('./database/settings');
var pass = require('pwd');
pass.iterations(100);

function addUser(username, password) {
    var passwd = Store('passwd');
    var userRecord = {}; 
    
    //overwrite the user
    pass.hash(password, function(err, salt, hash){
                userRecord.salt = salt;
                userRecord.hash = hash;
                passwd.set(username, userRecord);
            });
}

function authenticate(username,password, callback) {
    var passwd = Store('passwd');
    var userRecord = passwd.get(username);
    
    if(userRecord) {
        pass.hash(password, userRecord.salt, function(err, hash) {
            if(err) return(callback(err));

            //console.log(JSON.stringify(userRecord.hash));
            //console.log(JSON.stringify(hash));
            
            if(JSON.stringify(hash) == JSON.stringify(userRecord.hash) ) { //password ok
                callback(null,true);
            }
            else    //password wrong
                callback(null,false);
        });
    }    
    else { //user not found
        callback(null,false);
    }
}

exports.addUser = addUser;
exports.authenticate = authenticate;
