
/*
 * banner provider for arduino LCD
 */

//ministore 
var newsStore = require('ministore')('./database/news');
var settingsStore = require('ministore')('./database/settings');
var globals = settingsStore('globals');

var tsession = require("temboo/core/temboosession");
var session = new tsession.TembooSession(globals.get("temboo_USER"), globals.get("temboo_APPLICATION_NAME"), globals.get("temboo_APPKEY"));

//xml parsing required to read yahoo data
var select = require('xpath.js')
    , dom = require('xmldom').DOMParser;


var i = 0; //round robin


//returns banner
exports.get = function(req, res){
    var bannersStore = newsStore('banner');
    var banners =  bannersStore.get('banner');
    res.send(banners[ (i++) % banners.length ]);
    return;
};



//read Temboo Yahoo- weather and populate banner
exports.populate = function() {
    var Yahoo = require("temboo/Library/Yahoo/Weather");
    var getWeatherByAddressChoreo = new Yahoo.GetWeatherByAddress(session);

    // Instantiate and populate the input set for the choreo
    var getWeatherByAddressInputs = getWeatherByAddressChoreo.newInputSet();

    
    // Set inputs
    getWeatherByAddressInputs.set_Units("c");
    getWeatherByAddressInputs.set_Address(globals.get("address"));

    // Run the choreo, specifying success and error callback handlers
    getWeatherByAddressChoreo.execute(
        getWeatherByAddressInputs,
        function(resultsxml){ 
                var banners = newsStore('banner');
                var bannerArray =  new Array();
            
                var doc = new dom().parseFromString(resultsxml.get_Response()); 
                //temperature
                var nodes = select(doc, "/rss/channel/item/yweather:condition/@temp");
                bannerArray[0] = "Outside "+nodes[0].nodeValue+" C";
                globals.set('outside_temperature',parseInt(nodes[0].nodeValue));
                
                //weather description
                nodes = select(doc, "/rss/channel/item/yweather:condition/@text");
                bannerArray[1] = nodes[0].nodeValue;
                
                //forecast for today
                nodes_day = select(doc,"/rss/channel/item/yweather:forecast/@day");
                nodes_low = select(doc,"/rss/channel/item/yweather:forecast/@low");
                nodes_high= select(doc,"/rss/channel/item/yweather:forecast/@high");
                nodes_text = select(doc,"/rss/channel/item/yweather:forecast/@text");
                var index = 2;
                for(var i = nodes_day.length-1; /*i>=0*/ i >= nodes_day.length-2 ; i--) {
                    var day = nodes_day[i].nodeValue;
                    var low = nodes_low[i].nodeValue;
                    var high = nodes_high[i].nodeValue;
                    var text = nodes_text[i].nodeValue;
                    bannerArray[index++] = day+' '+low+'-'+high+' '+text;
                }
                banners.set('banner',bannerArray);
                
             
                },
        function(error){console.log(error.type); console.log(error.message);}
    );
    
}
