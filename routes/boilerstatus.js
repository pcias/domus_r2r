
/*
 * switcher of temperature source 
 */

//ministore 
    var Store = require('ministore')('./database/settings');



//returns json { boilier_connection_ok : true|false }
exports.get = function(req, res){
    var globalsStore = Store('globals');
    res.json(globalsStore.get("boiler_connection_ok"));
    return;
};

exports.heating = function(req,res) {
    var globalsStore = Store('globals');    
    res.json(globalsStore.get("heating"));
    return;
}

exports.global = function(req,res) {
    var globalsStore = Store('globals');    
    res.json(globalsStore.get("global"));
    return;
}