
/*
 * REST hours setting.
 */

 //ministore with temperature setting per hour
var Store = require('ministore')('./database/settings');


   



//expected to receive one hour input { hour : "0" ,  temp : "12" }
exports.write = function(req, res){
  var globalsStore = Store('globals');
  var hoursStore = Store(globalsStore.get('profile'));

  var newObj = {};    
  newObj['temp'] = req.body.temp;
  
  //unfortunately need to convert string to bool
  if (req.body.water=='true')
    newObj['water'] = true;
    else
    newObj['water'] = false;
    
    
  console.log(newObj);
  hoursStore.set(req.body.hour,newObj)
  
  res.json(true);
};