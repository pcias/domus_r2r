
/*
 * switcher of temperature source 
 */

//ministore 
var Store = require('ministore')('./database/settings')



//expected to receive  req = { [ actual_temperature : "21" ] , [ manual_temperature : "22" ] , [ temperature_source : "WEB|MAN|USU" ] , sensor_id = "..."}
exports.write = function(req, res){
    var actualTemp, manualTemp, tempSource;
    var date = new Date();
    var response = false; //assume failure
    var globalsStore = Store('globals');
    
     // validate data
    if(!req.body.sensor_id) {
        throw new Error('domus_r2:  Sensor data (POST) - wrong format');
        return;
    } 
     
    if(req.body.actual_temperature && !isNaN(actualTemp=parseInt(req.body.actual_temperature))) {
        globalsStore.set("actual_temperature",actualTemp);
        globalsStore.set("sensor_id",req.body.sensor_id);
        globalsStore.set("timestamp",date);
        response=true;
    }
    
    if(req.body.manual_temperature && !isNaN(manualTemp=parseInt(req.body.manual_temperature))) {
        globalsStore.set("manual_temperature",manualTemp);
        globalsStore.set("sensor_id",req.body.sensor_id);
        globalsStore.set("timestamp",date);
        response=true;
    }
    
    if  (req.body.temperature_source =="WEB" || req.body.temperature_source=="MAN" || req.body.temperature_source =="USU") {
        globalsStore.set("temperature_source",req.body.temperature_source);
        globalsStore.set("sensor_id",req.body.sensor_id);
        globalsStore.set("timestamp",date);
        response=true;
    }
    
    if  (req.body.profile =="HOME" || req.body.profile=="AWAY" ) {
        globalsStore.set("profile",req.body.profile);
        globalsStore.set("sensor_id",req.body.sensor_id);
        globalsStore.set("timestamp",date);
        response=true;
    }
    
    res.json(response);
    return;
};

//set of not expressive but simple APIs , just for arduino
exports.get_web_temperature = function(req,res) {
    var globalsStore = Store('globals');    
    res.json(globalsStore.get("web_temperature"));
    return;
}

exports.get_manual_temperature = function(req,res) {
    var globalsStore = Store('globals');    
    res.json(globalsStore.get("manual_temperature"));
    return;
}

exports.get_actual_temperature = function(req,res) {
    var globalsStore = Store('globals');    
    res.json(globalsStore.get("actual_temperature"));
    return;
}

exports.get_temperature_source = function(req,res) {
    var globalsStore = Store('globals');    
    res.send(globalsStore.get("temperature_source"));
    return;
}

exports.get_profile = function(req,res) {
    var globalsStore = Store('globals');    
    res.send(globalsStore.get('profile'));
    return;
}