
/*
 * temperature logger (neverending)
 */

//ministore 
var Store = require('ministore')('./database/log')
var templogStore = Store('templog');


//expectedd to receive  req = { actual_temperature : "21.0", manual_temperature : "22.0", temperature_source : "WEB|MAN|USU" , [humidity : "49.2"]}
exports.write = function(req, res){
    var actualTemp, manualTemp, tempSource, humidity;
     // validate data
    if((req.body.actual_temperature && isNaN(actualTemp=parseFloat(req.body.actual_temperature))) ||
       (req.body.manual_temperature && isNaN(manualTemp=parseFloat(req.body.manual_temperature))) ||
       (req.body.humidity && isNaN(humidity=parseFloat(req.body.humidity))) ||
       !req.body.sensor_id
       ) {
        throw new Error('domus_r2:  Sensor data (POST) - wrong format');
        res.json(false);
        return;
    }
    
    //everything ok
    var date = new Date();
    var record = {};
    record.actual_temperature = actualTemp;
    record.manual_temperature = manualTemp;
    record.temperature_source = req.body.temperature_source;
    if(req.body.humidity) { record.humidity = humidity; }
    record.sensor_id =  req.body.sensor_id;
    
    res.json(true);
    
    return templogStore.set(date,record);
};